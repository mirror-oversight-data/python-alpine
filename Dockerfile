FROM python:2-alpine

RUN apk add --update --no-cache \
        g++ \
        libxml2 \
        libxml2-dev \
		libxslt-dev 

RUN apk add --update --no-cache \
        bash \
		py-pip \
		graphviz

RUN pip install --no-cache-dir pytest graphviz lxml


